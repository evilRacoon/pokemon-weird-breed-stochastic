import java.lang.String.format

const val NUMBER_OF_STATS = 6
const val NUMBER_OF_INHERITED_STATS = 3
const val PARENT_1_PERFECT_STATS = 3
const val PARENT_2_PERFECT_STATS = 2
const val CHILD_REQUIRED_PERFECT_STATS = 2
val MODE = Breed.UNINHERITED_MODE.ALL_FALSE // ALL_FALSE or ONE_OF_32_IS_GOOD

fun main() {
    val parent1 = Pokemon.generateFixedPerfectStats(PARENT_1_PERFECT_STATS)
    val parent2 = Pokemon.generateFixedPerfectStats(PARENT_2_PERFECT_STATS)

    val children = mutableListOf<Pokemon>()
    val weightOneOuterIteration = 1.0 / parent1.size
    parent1.forEachIndexed { outerIndex, firstParent ->
        val outerProgress = outerIndex.toDouble() / parent1.size
        parent2.forEachIndexed { innerIndex, secondParent ->
            val innerProgress = innerIndex.toDouble() / parent2.size
            val totalProgressPercent = ((outerProgress + (innerProgress * weightOneOuterIteration)) * 100).toInt()

            println("$firstParent and $secondParent breed \t(Progress: $totalProgressPercent %)")
            val bred = Breed.breedPokemon(firstParent, secondParent, MODE)
            if (bred.size < 8) println("$bred") // print only if not too much
            else println("Finished with ${bred.size} childs")
            children.addAll(bred)
            println()
        }
    }
    println()

    val childrenWithPredicate = children.filter { it.perfectStat.count { it } >= CHILD_REQUIRED_PERFECT_STATS }
    println("Matching:")
    if (childrenWithPredicate.size < 32) println(childrenWithPredicate) // print only if not too much
    else println("Too many")
    println()

    println(
        format(
            "Total: %d. Matched: %d. Percentage: %f",
            children.count(),
            childrenWithPredicate.count(),
            childrenWithPredicate.count().toDouble() / children.count().toDouble()
        )
    )
}
