object Breed {
    enum class UNINHERITED_MODE(val uninheritedStats: List<Boolean>) {
        ALL_FALSE(listOf(false)),
        ONE_OF_32_IS_GOOD(BooleanArray(32) { index -> index == 31 }.toList())
    }

    fun breedPokemon(parent1: Pokemon, parent2: Pokemon, uninheritedMode: UNINHERITED_MODE): List<Pokemon> {
        val listOfPossibleChildren = mutableListOf<Pokemon>()
        val inheritedStatsPermutation = filterPermutationsWithTrue(NUMBER_OF_INHERITED_STATS)
        inheritedStatsPermutation.forEach { inheritedStats ->

            val stats = Array<List<Boolean>?>(NUMBER_OF_STATS) { null }
            inheritedStats.forEachIndexed { index, stat ->
                if (stat) stats[index] = listOf(parent1.perfectStat[index], parent2.perfectStat[index])
                else stats[index] = uninheritedMode.uninheritedStats
            }
            listOfPossibleChildren.addAll(flatten(stats))

        }
        return listOfPossibleChildren
    }

    fun flatten(
        possibleStats: Array<List<Boolean>?>,
        currentIndex: Int = 0,
        currentList: List<Pokemon> = emptyList()
    ): List<Pokemon> {
        return if (currentIndex == possibleStats.size) {
            currentList
        } else {
            val finalList = mutableListOf<Pokemon>()
            possibleStats[currentIndex]!!.forEach { statToChange: Boolean ->
                val currentListWithCurrentIndexSet =
                    if (currentList.isNotEmpty())
                        currentList.map { Pokemon(it.perfectStat.copyOf().apply { this[currentIndex] = statToChange }) }
                    else listOf(Pokemon().apply { this.perfectStat[currentIndex] = statToChange })
                finalList.addAll(flatten(possibleStats, currentIndex + 1, currentListWithCurrentIndexSet))
            }
            finalList
        }
    }
}
