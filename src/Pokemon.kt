data class Pokemon(
    val perfectStat: BooleanArray = BooleanArray(NUMBER_OF_STATS).apply { fill(false) }
) {
    init {
        require(perfectStat.size == NUMBER_OF_STATS)
    }

    companion object {
        fun generateFixedPerfectStats(perfectStats: Int): List<Pokemon> =
            filterPermutationsWithTrue(perfectStats).map { Pokemon(it.toBooleanArray()) }

    }
}
