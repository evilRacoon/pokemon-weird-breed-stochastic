import kotlin.math.pow
import kotlin.test.expect

val allPermutationsAsBooleans by lazy {
    val allPermutations = 0 until 2.0.pow(NUMBER_OF_STATS.toDouble()).toInt()
    val allPermutationsAsBinaryString =
        allPermutations.map { Integer.toBinaryString(it).padStart(NUMBER_OF_STATS, '0') }
    allPermutationsAsBinaryString.map {
        it.toCharArray().apply {
            expect(NUMBER_OF_STATS) { it.count() }
        }.map { it == '1' }
    }
}

fun filterPermutationsWithTrue(countOfTrue: Int): List<List<Boolean>> {
    return allPermutationsAsBooleans.filter { it.count { it } == countOfTrue }
}